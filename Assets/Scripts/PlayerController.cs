using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterController cc;
    public float moveSpeed;
    public float jumpForce;
    public float gravity;

    private Vector3 moveDir;
    private Animator anim;

    private bool isWalking = false;

    void Start()
    {
        anim = GetComponent<Animator>();
        cc = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        moveDir = new Vector3(Input.GetAxis("Horizontal") * moveSpeed, moveDir.y, Input.GetAxis("Vertical") * moveSpeed);
        moveDir.y -= gravity * Time.deltaTime;

        if (Input.GetButtonDown("Jump") && cc.isGrounded){
            moveDir.y = jumpForce;
        }

        if(moveDir.x != 0 || moveDir.z != 0)
        {
            isWalking = true;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(moveDir.x, 0, moveDir.z)), 0.15f);
        }else
        {
            isWalking = false;
        }

        anim.SetBool("IsWalking",isWalking);

        cc.Move(moveDir * Time.deltaTime);
    }
}
