using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Networking.PlayerConnection;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
    public static PlayerInfo playerInfo;

    public int playerHealth = 3;
    public int nbCoin = 0;

    public Image[] heartTab;
    public TextMeshProUGUI coinTxt;
    public TextMeshProUGUI scoreTxt;

    public CheckPointMgr checkPointMgr;

    public void Awake()
    {
        playerInfo = this;
    }

    public void SetHealth(int val)
    {
        playerHealth += val;
        if(playerHealth <= 0)
        {
            playerHealth = 0;
            checkPointMgr.Respawn();

        } 
        if(playerHealth > 3) { playerHealth = 3; }

        SetHealthBar();
    }

    public void AddCoin()
    {
        nbCoin++;
        coinTxt.text = nbCoin.ToString();
    }

    public void SetHealthBar()
    {
        foreach(Image img in heartTab)
        {
            img.enabled = false;
        }

        for(int i=0; i < playerHealth; i++)
        {
            heartTab[i].enabled = true;
        }
    }

    public int GetScore()
    {
        int scoreFinal = (nbCoin * 5) + (playerHealth * 10);
        scoreTxt.text = "Score: " + scoreFinal;
        return scoreFinal;
    }
}
