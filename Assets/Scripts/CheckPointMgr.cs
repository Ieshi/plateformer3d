using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointMgr : MonoBehaviour
{
    public Vector3 lastPosition;
    // Start is called before the first frame update
    void Start()
    {
        lastPosition = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "checkPoint")
        {
            lastPosition = transform.position;
            other.gameObject.GetComponent<FlowerAnim>().enabled = true;
        }
    }

    public void Respawn()
    {
        transform.position = lastPosition;
        PlayerInfo.playerInfo.SetHealth(3);
    }
}
