using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCollision : MonoBehaviour
{
    public GameObject pickUpEffect;
    public GameObject mobEffect;

    public GameObject cam1;
    public GameObject cam2;
    public GameObject cam3;

    private bool canInstanciate = true;

    public AudioClip hitSound;
    AudioSource audioSource;


    private bool isInvicible = false;
    public Renderer renderer;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "coin")
        {
            GameObject pickUpGo = Instantiate(pickUpEffect, other.transform.position, Quaternion.identity);
            Destroy(pickUpGo, 0.5f);

            PlayerInfo.playerInfo.AddCoin();

            Destroy(other.gameObject);
        }

        //Gestion des camera
        if (other.gameObject.tag == "cam1")
        {
            cam1.SetActive(true);
        }

        //Gestion des camera
        if (other.gameObject.tag == "cam2")
        {
            cam2.SetActive(true);
        }

        //Gestion des camera
        if (other.gameObject.tag == "cam3")
        {
            cam3.SetActive(true);
        }

        // End Game
        if (other.gameObject.name == "EndOfLvl")
        {
            print(PlayerInfo.playerInfo.GetScore());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //Gestion des camera
        if (other.gameObject.tag == "cam1")
        {
            cam1.SetActive(false);
        }

        //Gestion des camera
        if (other.gameObject.tag == "cam2")
        {
            cam2.SetActive(false);
        }
        //Gestion des camera
        if (other.gameObject.tag == "cam3")
        {
            cam3.SetActive(false);
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit collision)
    {
        if (collision.gameObject.tag == "hurt" && !isInvicible)
        {
            iTween.PunchPosition(gameObject, Vector3.back * 2, .5f);
            isInvicible = true;

            PlayerInfo.playerInfo.SetHealth(-1);

            StartCoroutine("ResetInvicible");
        }

        if (collision.gameObject.tag == "mob" && canInstanciate)
        {
            canInstanciate = false;

            audioSource.PlayOneShot(hitSound);

            iTween.PunchScale(collision.gameObject.transform.parent.gameObject, new Vector3(50.3f, 50.3f, 50.3f), 0.5f);


            GameObject mobpGo = Instantiate(mobEffect, collision.transform.position, Quaternion.identity);
            Destroy(mobpGo, 0.5f);
            Destroy(collision.gameObject.transform.parent.gameObject, 0.5f);
            StartCoroutine("ResetCanInstantiate");
        }

        if (collision.gameObject.tag == "autoDeath")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    IEnumerator ResetScene()
    {
        yield return new WaitForSeconds(0.8f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    IEnumerator ResetCanInstantiate()
    {
        yield return new WaitForSeconds(0.8f);
        canInstanciate = true;
    }

    IEnumerator ResetInvicible()
    {
        for(int i=0; i<10; i++)
        {
            yield return new WaitForSeconds(0.2f);
            renderer.enabled = !renderer.enabled;
        }

        yield return new WaitForSeconds(0.2f);
        renderer.enabled = true;
        isInvicible = false;
    }
}
